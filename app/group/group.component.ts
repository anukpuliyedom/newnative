import { Component, Input } from "@angular/core";
import { IGroup } from "passit-sdk-js/js/api.interfaces";

@Component({
  selector: "group-list",
  moduleId: module.id,
  templateUrl: "./group.component.html",
})
export class GroupComponent {
  @Input() groups: IGroup[];
}
