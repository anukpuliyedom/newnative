import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { GroupContainer } from "../passit-frontend/group/group.container";
import { GroupComponent } from "./group.component";
import { MobileMenuModule } from "../mobile-menu"
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { groupReducer } from "../passit-frontend/group/group.reducer";
import { GroupEffects } from "../passit-frontend/group/group.effects";
import { ContactsService } from "../passit-frontend/group/contacts/contacts.service";
import { GroupService } from "../passit-frontend/group/group.service";

export const COMPONENTS = [
  GroupContainer,
  GroupComponent,
];

@NgModule({
  imports: [
    NativeScriptCommonModule,
    MobileMenuModule,
    StoreModule.forFeature("group", groupReducer),
    EffectsModule.forFeature([GroupEffects])
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [ContactsService, GroupService],
	schemas: [NO_ERRORS_SCHEMA]
})
export class GroupModule { }
