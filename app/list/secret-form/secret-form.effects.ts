import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { RemoveSecretAction, SecretActionTypes } from "~/passit-frontend/secrets/secret.actions";
import { IS_TABLET } from "~/constants";
import { Router } from "@angular/router";
import { tap, filter } from "rxjs/operators";


@Injectable()
export class NSSecretFormEffects {
  @Effect({dispatch: false})
  removeSecret$ = this.actions$.pipe(
    ofType<RemoveSecretAction>(SecretActionTypes.REMOVE_SECRET),
    filter((action) => !IS_TABLET),
    tap(() =>
      this.router.navigate(['/list'])
    )
  );

  constructor(
    private actions$: Actions,
    private router: Router
  ) {}
}
