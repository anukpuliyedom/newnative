import { Component, Input } from "@angular/core";

@Component({
  selector: "progress-indicator",
  moduleId: module.id,
  templateUrl: "./progress-indicator.component.html",
})
export class ProgressIndicatorComponent {
  @Input() inProgress: boolean;
  @Input() inProgressText: string;
  @Input() completed: boolean;
  @Input() completedText: string;
  @Input() inputAction = false;
}
