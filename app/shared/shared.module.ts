import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NonFieldMessagesComponent } from "./non-field-messages/non-field-messages.component";
import { ProgressIndicatorComponent } from "./progress-indicator/progress-indicator.component";

export const COMPONENTS = [
  NonFieldMessagesComponent,
  ProgressIndicatorComponent
];

@NgModule({
  imports: [NativeScriptCommonModule],
  declarations: COMPONENTS,
  exports: COMPONENTS,
	schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule {}
