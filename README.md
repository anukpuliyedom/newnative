# Passit Mobile

Passit Mobile is a Nativescript app for Android (and maybe one day ios). It reuses the angular code from [passit-frontend](https://gitlab.com/passit/passit-frontend/)

Get the app now on [Google Play](https://play.google.com/store/apps/details?id=com.burkesoftware.Passit) or from [Gitlab CI](https://gitlab.com/passit/passit-mobile/pipelines).

We follow the redux principle of presentational vs smart components. Typically the app requires it's own module (so as to selectively import parts of the main app). The module just imports the frontend's reducers and smart components. Sometimes it extends them for mobile specific features. Then it provides the presentational componenents that output native elements instead of html.

## Development

Ensure you have nativescript [installed](https://docs.nativescript.org/angular/start/quick-setup) and a working Android SDK with emulator or device. If using Linux we recommend the [full installation instructions](https://docs.nativescript.org/angular/start/ns-setup-linux). Run `tns doctor` to ensure no errors come up.

If using Linux - edit `~/.bashrc` and add `export ANDROID_EMULATOR_USE_SYSTEM_LIBS=1` or else the emulator won't start.

After cloning the repo:

1. `git submodule init`
2. `git submodule update`
3. `npm i`
4. `tns run android`

## Troubleshooting

The world of Nativescript Mobile development is a wild one where things stop working for no apparent reason. If something isn't working try this:

- Stop tns run and start it again
- Run `./fix_sodium.sh` this script should run after npm i but doesn't always.
- Reinstall node packages `rm -rf node_modules` and `npm i`
- Remove and readd nativescript platform `tns platform remove android; tns platform add android`
- Update your Android SDK and emulator. Try deleting the emulator and remaking it.